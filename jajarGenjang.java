import java.util.Scanner;

public class jajarGenjang {
    public static void main(String[] args) {
        int angka;

        Scanner scan = new Scanner(System.in);

        System.out.print("Masukan angka = ");
        angka = scan.nextInt();

        int loki = angka - 1;

        for (int i = 0; i < angka; i++) {
            for (int space = loki; space >= 1; --space) {
                System.out.print("  ");
            }

            for (int j = 1; j <= angka; j++) {
                System.out.print(j + " ");
            }
            loki--;
            System.out.println("");
        }
    }

}